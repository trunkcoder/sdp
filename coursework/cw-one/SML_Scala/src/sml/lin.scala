package sml

/**
  * Created by DavidiMac on 06/03/2017.
  */
class lin (ins:Array[String]){

  def returnInstruction(): Instruction = {
    LinInstruction(ins(0), ins(2).toInt, ins(3).toInt)
  }
}

object lin {
  def apply(ins: Array[String]) = {
    new out(ins)
  }
}