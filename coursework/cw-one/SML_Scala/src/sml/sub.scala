package sml

/**
  * Created by DavidiMac on 06/03/2017.
  */
class sub (ins:Array[String]){
  def returnInstruction(): Instruction = {
    SubInstruction(ins(0), ins(2).toInt, ins(3).toInt, ins(4).toInt)
  }
}

object sub {

  def apply(ins: Array[String]) = {
    new sub(ins)
  }

}