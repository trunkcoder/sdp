package sml

/**
  * Created by DavidiMac on 08/03/2017.
  */
class bnz (ins:Array[String]){

  def returnInstruction(): Instruction = {
    BnzInstruction(ins(0), ins(2).toInt, ins(3))
  }
}

object bnz {
  def apply(ins: Array[String]) = {
    new out(ins)
  }
}