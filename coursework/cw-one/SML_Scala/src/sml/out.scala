package sml

/**
  * Created by DavidiMac on 08/03/2017.
  */
class out(ins:Array[String]){

  def returnInstruction(): Instruction = {
    OutInstruction(ins(0), ins(2).toInt)
  }
}

object out {
  def apply(ins: Array[String]) = {
    new out(ins)
  }
}
