package sml

/**
  * Created by DavidiMac on 08/03/2017.
  */
class mul(ins:Array[String]){


  def returnInstruction(): Instruction = {
    MulInstruction(ins(0), ins(2).toInt, ins(3).toInt, ins(4).toInt)
  }



}

object mul {

  def apply(ins: Array[String]) = {
    new mul(ins)
  }

}

