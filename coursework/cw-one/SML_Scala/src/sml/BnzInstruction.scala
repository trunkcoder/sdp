package sml

/**
  * Created by DavidiMac on 08/03/2017.
  */
case class BnzInstruction(label: String, opcode: String, register: Int, jumpLabel: String) extends Instruction(label, opcode) {

  var regVal = 0
  var index = 0
  var jumpIndex = 0
  var looped = 0

  override def execute(m: Machine) = {
    index = 0
    looped = 0
    regVal = m.regs(register)
    if (regVal != 0) {
      m.labels.labels.foreach{
        l =>{
          if(l == jumpLabel) jumpIndex = index
          index += 1
          looped += 1
        }

      }
      m.pc = jumpIndex
    }

println("Looped: " + looped)




    println("Jump label position is: " + jumpIndex)
    println("Label to jump to if not zero: " + jumpLabel)
    println("Register value: " + regVal)
    println("Labels: " + m.labels )
    println("PC: " + m.pc)
  }



  override def toString(): String = {
    super.toString + " register " + register + " value is " + "\n"
  }
}

object BnzInstruction {
  def apply(label: String, register: Int, jumpLabel: String) =
    new BnzInstruction(label, "bnz", register, jumpLabel)
}