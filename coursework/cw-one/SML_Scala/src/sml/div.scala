package sml

/**
  * Created by DavidiMac on 08/03/2017.
  */
class div (ins:Array[String]){


  def returnInstruction(): Instruction = {
    DivInstruction(ins(0), ins(2).toInt, ins(3).toInt, ins(4).toInt)
  }

}

object div {

  def apply(ins: Array[String]) = {
    new mul(ins)
  }

}

