package sml

/**
  * Created by DavidiMac on 05/03/2017.
  */
class add (ins:Array[String]){


  def returnInstruction(): Instruction = {
    AddInstruction(ins(0), ins(2).toInt, ins(3).toInt, ins(4).toInt)
  }



}

object add {

  def apply(ins: Array[String]) = {
    new add(ins)
  }

}
