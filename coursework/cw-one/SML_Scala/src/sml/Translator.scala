package sml

/*
 * The translator of a <b>S</b><b>M</b>al<b>L</b> program.
 */
class Translator(fileName: String) {
  private final val ADD = "add"
  private final val LIN = "lin"
  private final val BNZ = "bnz"
  private final val MUL = "mul"
  private final val SUB = "sub"
  private final val OUT = "out"
  private final val DIV = "div"

  /**
    * translate the small program in the file into lab (the labels) and prog (the program)
    */
  def readAndTranslate(m: Machine): Machine = {
    val labels = m.labels
    var program = m.prog
    import scala.io.Source
    val lines = Source.fromFile(fileName).getLines
    for (line <- lines) {
      val fields = line.split(" ")
      if (fields.length > 0) {
        labels.add(fields(0))
        fields(1) match {
          case ADD =>
            program = program :+ createClass(fields)
          case LIN =>
            program = program :+ createClass(fields)
          case SUB =>
            program = program :+ createClass(fields)
          case MUL =>
            program = program :+ createClass(fields)
          case DIV =>
            program = program :+ createClass(fields)
          case OUT =>
            program = program :+ createClass(fields)
          case BNZ =>
            program = program :+ createClass(fields)
          case x =>
            println(s"Unknown instruction $x")
        }
      }
    }
    new Machine(labels, program)
  }

  def createClass(instr: Array[String]): Instruction ={
    val className = "sml." + instr(1)
    val theClass = Class.forName(className)

    val theConstructor = theClass.getConstructor(classOf[Array[String]]).newInstance(instr)
    val method = theClass.getMethod("returnInstruction")
    method.invoke(theConstructor).asInstanceOf[Instruction]

  }
}


object Translator {
  def apply(file: String) = new Translator(file)
}
