package sml

abstract class Instruction(label: String, opcode: String) {

  override def toString(): String = label + ": " + opcode

  def getLabel = label

  def getOpcode = opcode

  def execute(m: Machine): Unit
}